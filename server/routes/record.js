const { response } = require('express');
const express = require('express');
const fetch = require('node-fetch');
// recordRoutes is an instance of the express router.
// We use it to define our routes.
// The router will be added as a middleware and will take control of requests starting with path /listings.
const recordRoutes = express.Router();

// This will help us connect to the database
const dbo = require('../db/conn');
let api_token = 'Bearer b95f5c6327eb90e4c4dc76b07527e9ba06682b60';

//Connect endpoint
recordRoutes.route('/connect/create-account').get(async function (_req, res) {

  // check user fron strava
  fetch('https://www.strava.com/api/v3/athlete', {
      method: 'GET',
      headers: { 
        'Authorization':api_token
      }
  }).then(res => res.json())
    .then((data) => {
      let authors = data;
      if(res.json.length == 1){
        return res.json(authors);
      }
      else{
        // if not exist create new one
          const dbConnect = dbo.getDb();
          const createAccount = {
            name:"andimuamar123",
            type:"1",
            start_date_local:"12",
            elapsed_time:"123",
            description:"123",
            distance:"123",
            trainer:"tes",
            commute:"123",
            hide_from_home:1
          };
  
          dbConnect
            .collection("accounts")
            .insertOne(createAccount, function (err, result) {
              if (err) {
                res.status(400).send("Error create account");
              } else {
                console.log(`Added a new account with id ${result.insertedId}`);
                res.status(204).send();
              }
            });
      }
    })
    .catch(err => console.log(err));
  
  
});
recordRoutes.route('/connect/logs-user-in').get(async function (_req, res) {

});
recordRoutes.route('/connect/authorize-app').get(async function (_req, res) {

});

// Disconnect endpoint
recordRoutes.route('/disconnect/logs-the-user-out').get(async function (_req, res) {

});
recordRoutes.route('/disconnect/deauthorize-app').get(async function (_req, res) {

});

// Sync endpoint
recordRoutes.route('/syncendpoint/user-activities').get(async function (_req, res) {

  fetch('https://www.strava.com/api/v3/athlete/activities?before=&after=&page=&per_page=3', {
        method: 'GET',
        headers: { 
          'Authorization':api_token
        }
    }).then(res => res.json())
      // .then(json => console.log(json))
      .catch(err => console.log(err));

});
recordRoutes.route('/syncendpoint/save-activities').get(async function (_req, res) {
  const activities = await
    fetch('https://www.strava.com/api/v3/athlete/activities?before=&after=&page=&per_page=3', {
        method: 'GET',
        headers: { 
          'Authorization':api_token
        }
    }).then(res => res.json())
      // .then(json => console.log(json))
      .catch(err => console.log(err));

    if(activities){

      const dbConnect = dbo.getDb();
      const activitiesPayload = {
        name:"andimuamar",
        type:"1",
        start_date_local:"12",
        elapsed_time:"123",
        description:"123",
        distance:"123",
        trainer:"tes",
        commute:"123",
        hide_from_home:1
      };

      dbConnect
        .collection("activities")
        .insertOne(activitiesPayload, function (err, result) {
          if (err) {
            res.status(400).send("Error inserting matches!");
          } else {
            console.log(`Added a new match with id ${result.insertedId}`);
            res.status(204).send();
          }
        });

    }
    else{
      console.log("cannot get data");
    }

});

// Accounts endpoints
recordRoutes.route('/accounts/list').get(async function (_req, res) {
  const dbConnect = dbo.getDb();

  dbConnect
    .collection("accounts")
    .find({}).limit(50)
    .toArray(function (err, result) {
      if (err) {
        res.status(400).send("Error fetching accounts!");
     } else {
        res.json(result);
      }
    });
});

// Account endpoint
recordRoutes.route('/account/(:id)').get(async function (_req, res) {
  const dbConnect = dbo.getDb();

  dbConnect
    .collection("accounts")
    .findById({id})
    .toArray(function (err, result) {
      if (err) {
        res.status(400).send("Error fetching accounts!");
     } else {
        res.json(result);
      }
    });
});

// Activities
recordRoutes.route('/activties/list').get(async function (_req, res) {
  const dbConnect = dbo.getDb();

  dbConnect
    .collection("activities")
    .find({}).limit(50)
    .toArray(function (err, result) {
      if (err) {
        res.status(400).send("Error fetching listings!");
     } else {
        res.json(result);
      }
    });
});
recordRoutes.route('/activties/:type/:user').get(async function (_req, res) {

});

// Activity
recordRoutes.route('/activity/:id').get(async function (_req, res) {
  const dbConnect = dbo.getDb();

  dbConnect
    .collection("activities")
    .findById({id})
    .toArray(function (err, result) {
      if (err) {
        res.status(400).send("Error fetching accounts!");
     } else {
        res.json(result);
      }
    });
});

// Delete activity
recordRoutes.route('/activity/delete').get(async function (_req, res) {
  const dbConnect = dbo.getDb();
  const listingQuery = { activity_id: req.body.id };

  dbConnect
    .collection("activities")
    .deleteOne(listingQuery, function (err, _result) {
      if (err) {
        res.status(400).send(`Error deleting listing with id ${listingQuery.listing_id}!`);
      } else {
        console.log("1 document deleted");
      }
    });
});


module.exports = recordRoutes;
